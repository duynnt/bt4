package com.example.btckth;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Adapter;
import android.widget.GridView;
import android.widget.ViewFlipper;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ViewFlipper viewFlipper;
    GridView gridView;
    profileapdater profileapdater;
    ArrayList<profile> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhxa();
        viewFlipper.setFlipInterval(3000);
        viewFlipper.setAutoStart(true);
        profileapdater = new profileapdater(R.layout.donggrip,this,arrayList);
        gridView.setAdapter(profileapdater);
        getdata();
    }

    private void getdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, "http://192.168.42.28/androidbt/select.php", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        arrayList.clear();
                        for (int i = 0 ; i<response.length(); i++){
                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                arrayList.add(new profile(
                                   jsonObject.getString("hinhanh"),
                                        jsonObject.getString("ten"),
                                        jsonObject.getString("thoigian")
                                ));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        profileapdater.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }

    private void anhxa() {
        viewFlipper = (ViewFlipper) findViewById(R.id.vf);
        gridView = (GridView) findViewById(R.id.gv);
        arrayList = new ArrayList<>();
    }
}