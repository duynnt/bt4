package com.example.btckth;

public class profile {
    private String hinh;
    private String ten;
    private String time;

    public profile(String hinh, String ten, String time) {
        this.hinh = hinh;
        this.ten = ten;
        this.time = time;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
